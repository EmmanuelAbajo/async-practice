package com.example.executor.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.executor.api.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

}
