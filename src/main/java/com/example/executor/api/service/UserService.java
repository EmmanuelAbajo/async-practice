package com.example.executor.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.executor.api.entity.User;
import com.example.executor.api.repository.UserRepository;



@Service
public class UserService {
	
	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Async
	public CompletableFuture<List<User>> saveUser(MultipartFile file){
		long start = System.currentTimeMillis();
		List<User> users = parseCSVFile(file);
		log.info("Saving list of users of size {}",users.size()+" by "+Thread.currentThread().getName());
		users = userRepository.saveAll(users);
		long end = System.currentTimeMillis();
		log.info("Processing time: {}ms",(end - start));
		return CompletableFuture.completedFuture(users);
	}
	
	@Async
	public CompletableFuture<List<User>> getAllUsers(){
		log.info("Getting list of users by "+ Thread.currentThread().getName());
		List<User> users = userRepository.findAll();
		return CompletableFuture.completedFuture(users);
	}
	
	private List<User> parseCSVFile(final MultipartFile file) {
		final List<User> users = new ArrayList<>();
		BufferedReader reader = null;
		try {
       	 	reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String line;
            while((line = reader.readLine()) != null) {
            	final String[] data = line.split(",");
            	User user = new User();
            	user.setName(data[0]);
            	user.setEmail(data[1]);
            	user.setGender(data[2]);
            	users.add(user);
            }
    
       } catch (IOException io) {
    	   	log.error(io.getLocalizedMessage());
       		io.printStackTrace();
       } finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
       return users;
	}

}
