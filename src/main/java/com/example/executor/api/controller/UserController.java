package com.example.executor.api.controller;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.executor.api.entity.User;
import com.example.executor.api.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping(value="/users", consumes= {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = "application/json")
	public ResponseEntity<?> saveUsers(@RequestParam(value = "files") MultipartFile[] files) {
		for (MultipartFile file: files) {
			userService.saveUser(file);
		}
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@GetMapping(value="/users", produces="application/json")
	public CompletableFuture<ResponseEntity<?>> findAllUsers() {
		return userService.getAllUsers().thenApply(ResponseEntity::ok);
	}
	
	@GetMapping(value="/usersByThread", produces="application/json")
	public ResponseEntity<?> getAllUsers() {
		CompletableFuture<List<User>> users1 = userService.getAllUsers();
		CompletableFuture<List<User>> users2 = userService.getAllUsers();
		CompletableFuture<List<User>> users3 = userService.getAllUsers();

		CompletableFuture.allOf(users1,users2,users3).join();
		return ResponseEntity.status(HttpStatus.OK).build();
	}
}
